<?php

namespace AppBundle\Service;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;

class RedditProvider
{
    const URL_API_REDDIT = "https://www.reddit.com/";

    public function getPosts($term, $limit = 20, $offset = 0)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL_API_REDDIT."search.json?q=$term&limit=$limit&count=$offset&type=link");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);

        $posts = [];
        $data = json_decode($response, true);
        if ($data["kind"] == "Listing" && !empty($data["data"]["children"])) {
            foreach ($data["data"]["children"] as $item) {
                if ($item["kind"] == "t3") {
                    $dt = new \DateTime();
                    $posts[] = new Post(
                        $item["data"]["id"],
                        $item["data"]["author"],
                        clone($dt->setTimestamp($item["data"]["created_utc"])),
                        $item["data"]["url"]
                    );
                }
            }
        }

        return $posts;
    }

    public function getComments($postId, $limit = 20, $offset = 0)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL_API_REDDIT."comments/$postId.json?limit=$limit&depth=$offset");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);

        $comments = [];
        $data = json_decode($response, true);
        if (count($data) == 2 && $data[1]["kind"] == "Listing" && !empty($data[1]["data"]["children"])) {
            foreach ($data[1]["data"]["children"] as $item) {
                if ($item["kind"] == "t1") {
                    $dt = new \DateTime();
                    $comments[] = new Comment(
                        $item["data"]["id"],
                        $postId,
                        $item["data"]["author"],
                        clone($dt->setTimestamp($item["data"]["created_utc"])),
                        $item["data"]["body"]
                    );
                }
            }
        }

        return $comments;
    }
}