<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class RedditController extends FOSRestController
{
    /**
     * @Rest\View()
     */
    public function postPostsAction(Request $request, $term)
    {
        $limit = $this->get('request')->request->get('limit');
        $offset = $this->get('request')->request->get('offset');

        $limit = $limit ? $limit : 20;
        $offset = $offset ? $offset : 0;

        $posts = $this->get("redditProvider")->getPosts($term, $limit , $offset);
        return $posts;
    }

    /**
     * @Rest\View()
     */
    public function postCommentsAction(Request $request, $postId)
    {
        $limit = $this->get('request')->request->get('limit');
        $offset = $this->get('request')->request->get('offset');

        $limit = $limit ? $limit : 20;
        $offset = $offset ? $offset : 0;
        
        $comments = $this->get("redditProvider")->getComments($postId, $limit , $offset);
        return $comments;
    }
}
