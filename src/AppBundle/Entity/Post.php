<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class Post
 * @package AppBundle\Entity
 */
class Post
{
    /**
     * @var string
     * @SerializedName("postId")
     */
    private $id;
    /**
     * @var string
     */
    private $author;
    /**
     * @var \DateTime
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $date;
    /**
     * @var string
     * @SerializedName("postContent")
     */
    private $content;

    /**
     * Post constructor.
     * @param int $id
     * @param string $author
     * @param \DateTime $date
     * @param string $content
     */
    public function __construct($id, $author, \DateTime $date, $content)
    {
        $this->id = $id;
        $this->author = $author;
        $this->date = $date;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}