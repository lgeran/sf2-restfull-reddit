<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class Comment
 * @package AppBundle\Entity
 */
class Comment
{

    /**
     * @var string
     * @SerializedName("comentId")
     */
    private $id;
    /**
     * @var string
     */
    private $postId;
    /**
     * @var string
     */
    private $author;
    /**
     * @var \DateTime
     * @Serializer\Type("DateTime<'d/m/Y H:i'>")
     */
    private $date;
    /**
     * @var string
     * @SerializedName("commentContent")
     */
    private $content;

    /**
     * Comment constructor.
     * @param string $id
     * @param string $postId
     * @param string $author
     * @param \DateTime $date
     * @param string $content
     */
    public function __construct($id, $postId, $author, \DateTime $date, $content)
    {
        $this->id = $id;
        $this->postId = $postId;
        $this->author = $author;
        $this->date = $date;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPostId()
    {
        return $this->postId;
    }


    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}